# Dubai Customs 'AI Hackathon'
Building a machine learning model to predict the 8-digit “HS Code” based on a Customer Provided HS Codes dataset




# :star: How to run the code :star:
- Open **Predict.ipynb** using Jupyter Notebook

- Run the first cell to import all the required libraries and models 

	![image](images/cell1.png)

- The **dc_utilites.py** contains the functions and loads the required models

- The list in the second cell **keywordsList** contains the keywords (input) which we'll predict

- The predict_hscodes function accepts a list of keyword and return a list of predicted codes

	[image](images/function.png)

- You can use the **showOutput** to print the input and the codes while runing the **predict_hscodes** function
	- **showOutput = False** will not print anything

	![image](images/false.png)	
	
	- **showOutput = True** will print the input and the predicted code

	![image](images/true.png)

- Please note that files and folders should by in the same strucutre 
	![image](images/strucutre.png)




## Data Structure 
- After we analysed the data and understood the why the codes were generated we were sure that it’s not a classification problem

- The codes were generated  using specific pattern **Section** => **Chapter** => **Heading** => **Subheading**

- We are facing a Deep Learning, NLP, Sequence-to-Sequence (**Seq2Seq**) problem, convert a sequences from one domain (sequences of words) to sequences in another domain (sequences of codes [section, chapter, heading, subheading].)

- Recurrent Neural Networks or more precisely LSTM (Long Short-Term Memory) is very effective and useful to solve our problem

- The most common architecture used to build Seq2Seq models is the Encoder Decoder architecture, Both encoder and the decoder are typically LSTM models

- Encoder reads the input sequence and summarizes the information in something called as the internal state vectors.

- Decoder is an LSTM whose initial states are initialized to the final states of the Encoder LSTM. Using these initial states, decoder starts generating the output sequence.


## Seq2Seq LSTM Model
![image](images/encode-decode.png)
