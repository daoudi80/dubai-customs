#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 26 13:26:53 2019

@author: daoudi
"""

import pandas as pd
import numpy as np

import string
from string import digits
import matplotlib.pyplot as plt
import re
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from keras.layers import Input, LSTM, Embedding, Dense
from keras.models import Model
from random import shuffle
from keras.callbacks import EarlyStopping
from keras.models import load_model

import tensorflow as tf
from tensorflow.keras.backend import set_session

def cleanInputText(text):
    text = text.lower()       
    text = re.sub('[^A-Za-z0-9]+', ' ', text)
    return text.strip()

# Doing a cleaning of the output texts - len should be 8 
def cleanOutputText(text, charNum = 0):
    # add zeros if length less than 8
    output = str(text).zfill(8)
    if charNum == 0:
        output = " ".join(output)
    else:
        # add spaces every 2 chars ()
        output = ' '.join(a+b for a,b in zip(output[::charNum], output[1::charNum]))             
    return output


def decode_sequence(input_seq):
    # Encode the input as state vectors.
    states_value = encoder_model.predict(input_seq)
    
    # Generate empty target sequence of length 1.
    target_seq = np.zeros((1,1))
    # Populate the first character of target sequence with the start character.
    target_seq[0, 0] = target_token_index['START_']

    # Sampling loop for a batch of sequences
    # (to simplify, here we assume a batch of size 1).
    stop_condition = False
    decoded_sentence = ''
    while not stop_condition:
        output_tokens, h, c = decoder_model.predict([target_seq] + states_value)

        # Sample a token
        sampled_token_index = np.argmax(output_tokens[0, -1, :])
        sampled_char = reverse_target_char_index[sampled_token_index]
        decoded_sentence += ' '+sampled_char

        # Exit condition: either hit max length
        # or find stop character.
        if (sampled_char == '_END' or
           len(decoded_sentence) > 50):
            stop_condition = True

        # Update the target sequence (of length 1).
        target_seq = np.zeros((1,1))
        target_seq[0, 0] = sampled_token_index

        # Update states
        states_value = [h, c]   
    return decoded_sentence.strip()

def predict_hscodes(hscodeList, showOuput = True):
    hscodeListY = []
    for row in hscodeList:
        hscodeListY.append('00 00 00 00')

    values = {'text': hscodeList, 'code': hscodeListY}   
    lines = pd.DataFrame(values) 
    lines.code = lines.code.apply(lambda x: 'START_ '+ x + ' _END')
    
    
    train_gen = generate_batch(lines.text, lines.code, batch_size = 1)
    
    target_hscodes = []
    for k in range(len(hscodeList)):
        (input_seq, actual_output), _ = next(train_gen)
        decoded_sentence = decode_sequence(input_seq)
        predictedCode = decoded_sentence[:-4]
        target_hscodes.append(predictedCode)
        if showOuput:
            print('Input: ', lines.text[k:k+1].values[0])
            print('Predicted Code: ', predictedCode)
            print('========================================================')
       
    return target_hscodes

    

def generate_batch(X, y, batch_size = 128):
    ''' Generate a batch of data '''
    while True:
        for j in range(0, len(X), batch_size):
            encoder_input_data = np.zeros((batch_size, max_length_src),dtype='float32')
            decoder_input_data = np.zeros((batch_size, max_length_tar),dtype='float32')
            decoder_target_data = np.zeros((batch_size, max_length_tar, num_decoder_tokens),dtype='float32')
            for i, (input_text, target_text) in enumerate(zip(X[j:j+batch_size], y[j:j+batch_size])):
                for t, word in enumerate(input_text.split()):
                    if word in input_token_index:
                        encoder_input_data[i, t] = input_token_index[word] # encoder input seq
                    if word in target_token_index:
                        for t, word in enumerate(target_text.split()):
                            if t<len(target_text.split())-1:
                                decoder_input_data[i, t] = target_token_index[word] # decoder input seq
                            if t>0:
                                # decoder target sequence (one hot encoded)
                                # does not include the START_ token
                                # Offset by one timestep
                                decoder_target_data[i, t - 1, target_token_index[word]] = 1.
            yield([encoder_input_data, decoder_input_data], decoder_target_data)

         
df = pd.read_csv('clean-data/multiClassesSubList1000000.csv')

codesList = df.values.tolist()

input_texts = []
code_texts = []
for row in codesList:
    desc = cleanInputText(row[1])
#     code = cleanOutputText(row[1], 2)
    code = row[2]
    input_texts.append(desc)
    code_texts.append(code)


values = {'text': input_texts, 'code': code_texts}   
lines = pd.DataFrame(values) 
lines.code = lines.code.apply(lambda x: 'START_ '+ x + ' _END')

#     print(lines.code)
#     print(lines.text)

all_text_words=set()
for t in lines.text:
  for word in t.split():
    if word not in all_text_words:
      all_text_words.add(word)


# Digits of Codes
all_code_digists=set()
for t in lines.code:
  for d in t.split():
    if d not in all_code_digists:
      all_code_digists.add(d)

# Max Length of source sequence
lenght_list=[]
for l in lines.text:
    lenght_list.append(len(l.split(' ')))
max_length_src = np.max(lenght_list)


# Max Length of target sequence
lenght_list=[]
for l in lines.code:
    lenght_list.append(len(l.split(' ')))
max_length_tar = np.max(lenght_list)



input_words = sorted(list(all_text_words))
target_words = sorted(list(all_code_digists))


# Calculate Vocab size for both source and target
num_encoder_tokens = len(all_text_words)
num_encoder_tokens += 1
num_decoder_tokens = len(all_code_digists)
num_decoder_tokens += 1 # For zero padding




# Create word to token dictionary for both source and target
input_token_index = dict([(word, i+1) for i, word in enumerate(input_words)])
target_token_index = dict([(word, i+1) for i, word in enumerate(target_words)])




# Create token to word dictionary for both source and target
reverse_input_char_index = dict((i, word) for word, i in input_token_index.items())
reverse_target_char_index = dict((i, word) for word, i in target_token_index.items())


# lines = shuffle(lines)
# Train - Test Split
X, y = lines.text, lines.code
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, shuffle=False) # formal data

# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.5) # customer data
X_train.shape, X_test.shape

# # X_train.to_pickle('X_train.pkl')
# # X_test.to_pickle('X_test.pkl')

latent_dim = 50


# Encoder
encoder_inputs = Input(shape=(None,))
enc_emb =  Embedding(num_encoder_tokens, latent_dim, mask_zero = True)(encoder_inputs)
encoder_lstm = LSTM(latent_dim, return_state=True)
encoder_outputs, state_h, state_c = encoder_lstm(enc_emb)
# We discard `encoder_outputs` and only keep the states.
encoder_states = [state_h, state_c]


# Set up the decoder, using `encoder_states` as initial state.
decoder_inputs = Input(shape=(None,))
dec_emb_layer = Embedding(num_decoder_tokens, latent_dim, mask_zero = True)
dec_emb = dec_emb_layer(decoder_inputs)
# We set up our decoder to return full output sequences,
# and to return internal states as well. We don't use the
# return states in the training model, but we will use them in inference.
decoder_lstm = LSTM(latent_dim, return_sequences=True, return_state=True)
decoder_outputs, _, _ = decoder_lstm(dec_emb,
                                     initial_state=encoder_states)
decoder_dense = Dense(num_decoder_tokens, activation='softmax')
decoder_outputs = decoder_dense(decoder_outputs)

model = Model([encoder_inputs, decoder_inputs], decoder_outputs)
model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['acc'])
model.load_weights('dc_weights_1000000_1024.h5', by_name=False)

train_samples = len(X_train)
val_samples = len(X_test)
batch_size = 2048
# batch_size = 1024
epochs = 100




# Encode the input sequence to get the "thought vectors"
encoder_model = Model(encoder_inputs, encoder_states)

# Decoder setup
# Below tensors will hold the states of the previous time step
decoder_state_input_h = Input(shape=(latent_dim,))
decoder_state_input_c = Input(shape=(latent_dim,))
decoder_states_inputs = [decoder_state_input_h, decoder_state_input_c]

dec_emb2= dec_emb_layer(decoder_inputs) # Get the embeddings of the decoder sequence

# To predict the next word in the sequence, set the initial states to the states from the previous time step
decoder_outputs2, state_h2, state_c2 = decoder_lstm(dec_emb2, initial_state=decoder_states_inputs)
decoder_states2 = [state_h2, state_c2]
decoder_outputs2 = decoder_dense(decoder_outputs2) # A dense softmax layer to generate prob dist. over the target vocabulary

#Final decoder model
decoder_model = Model(
    [decoder_inputs] + decoder_states_inputs,
    [decoder_outputs2] + decoder_states2)
